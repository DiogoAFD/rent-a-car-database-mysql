use rentacar;

SET SQL_SAFE_UPDATES = 0;

INSERT INTO Cliente
	(idCliente, Nome, Contribuinte, Telemovel, Email, CodPostal, Rua, Localidade)
    VALUES
    (1,'Jacinto Pereira da Silva', '587412358', '915874562','jacinto_123@gmail.com','4700-584 Braga','Rua das Brocas, 420','Maximinos'),
    (2,'Jose Manuel Paiva','412578962','937542189','jose_123@gmail.com','4700-853 Braga','Rua da Brita, 215','Real'),
    (3,'Abelino Matos e Sousa','548874522','914522799','abelino_matos@hotmail.com','4770-123 Famalicão','Praça da Loucura, 1337','Requião'),
    (4,'Iolanda Magalhães Pires','455124478','924512876','iol_anda@gmail.com','4760-354 Trofa','Avenida da Fisga, 78','Moure'),
    (5,'Adriana Gonçalves Oliverira','451236987','932154879','ago_123@sapo.pt','4712-159 Leixões','Rua Francisco Rodrigues Junior, 13','Granja Alvorada'),
    (6,'Fernando Rocha Machado','321857946','936598784','rocha_machado@gmail.com','4852-852 Penha','Rua dos Bacalhoeiros, 852','Alfândega');
    
INSERT INTO Tipo
	(idTipo, Designacao, PrecoBase)
    VALUES
    (1,'Familiar',18.53),
    (2,'Desportivo',50.68),
    (3,'Citadino',9.47),
    (4,'Automático',23.55),
    (5,'Luxo',40.64),
    (6,'Classico',30.56);
    
INSERT INTO Veiculo
	(Matricula, Tipo ,Modelo, Marca, Categoria, Quilometragem, Observacoes)
    VALUES
    ('90-LS-50',3,'Corsa','Opel','B',135420.65,'Como novo!'),
    ('89-UN-43',3,'Golf','Volkswagen','B',171143.356,'Bom estado!'),
    ('39-EN-24',1,'Astra','Opel','B',50943.1,''),
    ('25-33-XQ',2,'M3 E92','BMW','B',80560.24,'Do AÇO!'),
    ('85-DH-87',5,'755i','BMW','B',110456.85,'Prazer e Conforto!'),
    ('45-DS-54',4,'S370','Mercedes','B', 126472.65, 'Prazer de Excelencia!'),
    ('76-PF-70',6,'320i E30','BMW','B',142301.42,'Clássico!'),
    ('PG-08-70',6,'4L','Renault','B',298235.147,'Pequeno grande intemporal!');
    
INSERT INTO Aluguer
	(idAluguer,Cliente,Veiculo,DataInicio,DataFim,DataEntrega,PrecoFinal)
	VALUES
    (1,1,'39-EN-24','2015-05-01','2015-06-01',NULL,NULL),
    (2,6,'76-PF-70','2016-01-01','2015-01-02','2015-01-02',30.56),
    (3,3,'89-UN-43','2016-01-03','2015-01-10','2015-01-10',94.7),
    (4,3,'90-LS-50','2016-01-05','2015-01-08','2015-01-08',28.41),
    (5,2,'89-UN-43','2014-03-13','2014-03-31','2014-03-31',170.46);

SET SQL_SAFE_UPDATES = 1;