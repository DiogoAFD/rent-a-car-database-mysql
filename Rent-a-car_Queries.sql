use rentacar;


-- Listar todos os carros (Marca,Modelo,Quilometragem) que foram alugados 
-- de um determinado tipo, ordenado decrescentemente pela quilometragem.

DELIMITER $$
CREATE PROCEDURE proc_VeiculosAlugadosPorTipo(IN tipo INT)

BEGIN

SELECT V.Marca, V.Modelo, V.Quilometragem
	FROM Veiculo AS V INNER JOIN Aluguer AS A
		ON V.Matricula = A.Veiculo
		INNER JOIN Tipo AS T
			ON T.idTipo = V.Tipo
	WHERE T.idTipo = tipo
    ORDER BY V.Quilometragem DESC;

END $$

DELIMITER ;

CALL proc_VeiculosAlugadosPorTipo(1);


-- Listar todos os carros (Marca,Modelo,Quilometragem)
-- de um determinado tipo, ordenado decrescentemente pela quilometragem.

DELIMITER $$
CREATE PROCEDURE proc_VeiculosPorTipo(IN tipo INT)

BEGIN

SELECT V.Marca, V.Modelo, V.Quilometragem
	FROM Veiculo AS V INNER JOIN Tipo AS T
		ON T.idTipo = V.Tipo
	WHERE T.idTipo = tipo
    ORDER BY V.Quilometragem DESC;

END $$

DELIMITER ;

CALL proc_VeiculosPorTipo(3);


-- Mostra quantos clientes alugaram um determinado veiculo

DELIMITER $$
CREATE PROCEDURE proc_cClientesAlugaramVeiculo(IN veiculo VARCHAR(45))

BEGIN

SELECT COUNT(C.idCliente)
	FROM Cliente AS C INNER JOIN Aluguer AS A
		ON C.idCliente = A.Cliente
        INNER JOIN Veiculo AS V
			ON V.Matricula = A.Veiculo
	WHERE V.Matricula = veiculo;

END $$

DELIMITER ;

CALL proc_cClientesAlugaramVeiculo('89-UN-43');


-- Mostra quantos carros um dado cliente alugou.

DELIMITER $$
CREATE PROCEDURE proc_cVeiculosAlugadosPorCliente(IN cliente INT)

BEGIN

SELECT COUNT(V.Matricula)
	FROM Veiculo AS V INNER JOIN Aluguer AS A
		ON V.Matricula = A.Veiculo
        INNER JOIN Cliente AS C 
			ON C.idCliente = A.Cliente
	WHERE C.idCliente = cliente;

END $$

DELIMITER ;

CALL proc_cVeiculosAlugadosPorCliente(1);


-- Transação de encerramento de aluguer

DELIMITER $$
CREATE PROCEDURE proc_EncerrarAluguer
(IN aluguer INT,IN tipo INT,IN dataEntrega DATE,IN km FLOAT)

BEGIN

DECLARE dataI DATE;
DECLARE dataF DATE;
DECLARE datadif INT;
DECLARE preco FLOAT;
DECLARE multa FLOAT;
DECLARE Erro BOOL DEFAULT 0;
DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET Erro = 1;

START TRANSACTION;

SET SQL_SAFE_UPDATES = 0;

SET dataI = (SELECT DataInicio FROM Aluguer WHERE idAluguer = aluguer);
SET dataF = (SELECT DataFim FROM Aluguer WHERE idAluguer = aluguer);
SET preco = (SELECT PrecoBase FROM Tipo WHERE idTipo = tipo);
SET multa = (DATEDIFF(dataEntrega,dataF)*10);

UPDATE Aluguer SET 
	DataEntrega = dataEntrega,
    PrecoFinal = (preco*DATEDIFF(dataF,dataI))+multa
WHERE idAluguer = aluguer;

UPDATE Veiculo SET Quilometragem = Quilometragem + km;

SET SQL_SAFE_UPDATES = 1;

IF Erro THEN ROLLBACK;
ELSE COMMIT;
END IF;

END $$

DELIMITER ;

CALL proc_EncerrarAluguer(1,1,'2015-06-02',80);


-- Transação que insere um novo Tipo e associa um Veiculo a esse tipo

DELIMITER $$
CREATE PROCEDURE tr_InsertTipoVeiculo
(IN tipo INT,IN des VARCHAR(45),IN preco FLOAT,IN mat VARCHAR(45),IN modelo VARCHAR(45),
IN marca VARCHAR(45),IN cat VARCHAR(45),IN quilo FLOAT, IN obs TEXT)

BEGIN 
DECLARE Erro BOOL DEFAULT 0;
DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET Erro = 1;
START TRANSACTION;

INSERT INTO Tipo(idTipo,Designacao,PrecoBase)
	VALUES(tipo,des,preco);

INSERT INTO Veiculo(Matricula,Tipo,Modelo,Marca,Categoria,Quilometragem,Observacoes)
	VALUES(mat,tipo,modelo,marca,cat,quilo,obs);

IF Erro THEN ROLLBACK;
	ELSE COMMIT;
END IF;

END $$

DELIMITER ;

call tr_InsertTipoVeiculo
(7,'Motociclo',6.50,'25-AC-22','XT125Z','Yamaha','A1',9245.78,NULL);