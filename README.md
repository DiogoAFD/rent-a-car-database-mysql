# **Rent-a-Car** #
###  ###
This project consists on the construction of a database, following the various steps for its realization:

* conceptual model
* logical model
* physical model

This database was developed to store information relating to a Rent-a-Car application.