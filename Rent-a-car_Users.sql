use rentacar;

SET SQL_MODE = '';
GRANT USAGE ON *.* TO admin;
DROP USER admin;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'admin' IDENTIFIED BY 'admin';

GRANT ALL ON `rentacar`.* TO 'admin';

SET SQL_MODE = '';
GRANT USAGE ON *.* TO cliente;
DROP USER cliente;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'cliente' IDENTIFIED BY 'cliente';

GRANT SELECT ON TABLE `rentacar`.`Cliente` TO 'cliente';
GRANT SELECT ON TABLE `rentacar`.`Aluguer` TO 'cliente';
GRANT SELECT ON TABLE `rentacar`.`Veiculo` TO 'cliente';
GRANT SELECT ON TABLE `rentacar`.`Tipo` TO 'cliente';
