-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema rentacar
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `rentacar` ;

-- -----------------------------------------------------
-- Schema rentacar
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `rentacar` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `rentacar` ;

-- -----------------------------------------------------
-- Table `rentacar`.`Cliente`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rentacar`.`Cliente` ;

CREATE TABLE IF NOT EXISTS `rentacar`.`Cliente` (
  `idCliente` INT NOT NULL COMMENT '',
  `Nome` VARCHAR(45) NOT NULL COMMENT '',
  `Contribuinte` VARCHAR(45) NOT NULL COMMENT '',
  `Telemovel` VARCHAR(45) NOT NULL COMMENT '',
  `Email` VARCHAR(45) NOT NULL COMMENT '',
  `CodPostal` VARCHAR(45) NOT NULL COMMENT '',
  `Rua` VARCHAR(45) NOT NULL COMMENT '',
  `Localidade` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`idCliente`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rentacar`.`Tipo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rentacar`.`Tipo` ;

CREATE TABLE IF NOT EXISTS `rentacar`.`Tipo` (
  `idTipo` INT NOT NULL COMMENT '',
  `Designacao` VARCHAR(45) NOT NULL COMMENT '',
  `PrecoBase` FLOAT NOT NULL COMMENT '',
  PRIMARY KEY (`idTipo`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rentacar`.`Veiculo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rentacar`.`Veiculo` ;

CREATE TABLE IF NOT EXISTS `rentacar`.`Veiculo` (
  `Matricula` VARCHAR(45) NOT NULL COMMENT '',
  `Tipo` INT NOT NULL COMMENT '',
  `Modelo` VARCHAR(45) NOT NULL COMMENT '',
  `Marca` VARCHAR(45) NOT NULL COMMENT '',
  `Categoria` VARCHAR(45) NOT NULL COMMENT '',
  `Quilometragem` FLOAT NOT NULL COMMENT '',
  `Observacoes` TEXT NULL COMMENT '',
  PRIMARY KEY (`Matricula`)  COMMENT '',
  INDEX `fk_Veiculo_Tipo1_idx` (`Tipo` ASC)  COMMENT '',
  CONSTRAINT `fk_Veiculo_Tipo1`
    FOREIGN KEY (`Tipo`)
    REFERENCES `rentacar`.`Tipo` (`idTipo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rentacar`.`Aluguer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rentacar`.`Aluguer` ;

CREATE TABLE IF NOT EXISTS `rentacar`.`Aluguer` (
  `idAluguer` INT NOT NULL COMMENT '',
  `Cliente` INT NOT NULL COMMENT '',
  `Veiculo` VARCHAR(45) NOT NULL COMMENT '',
  `DataInicio` DATE NOT NULL COMMENT '',
  `DataFim` DATE NOT NULL COMMENT '',
  `DataEntrega` DATE NULL COMMENT '',
  `PrecoFinal` FLOAT NULL COMMENT '',
  PRIMARY KEY (`idAluguer`)  COMMENT '',
  INDEX `fk_Aluger_Cliente_idx` (`Cliente` ASC)  COMMENT '',
  INDEX `fk_Aluger_Veiculo1_idx` (`Veiculo` ASC)  COMMENT '',
  CONSTRAINT `fk_Aluger_Cliente`
    FOREIGN KEY (`Cliente`)
    REFERENCES `rentacar`.`Cliente` (`idCliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Aluger_Veiculo1`
    FOREIGN KEY (`Veiculo`)
    REFERENCES `rentacar`.`Veiculo` (`Matricula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
